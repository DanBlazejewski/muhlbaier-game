#ifndef _HAL_SETTINGS_H_
#define	_HAL_SETTINGS_H_

// hint: the default clock for the MSP430F5529 is 1048576
// the default clock for the PIC32MX is set by configuration bits
#define FCPU     8000000L
// if peripheral clock is slower than main clock change it here
#define PERIPHERAL_CLOCK FCPU

#endif	// HAL_SETTINGS_H