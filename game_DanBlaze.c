
/*
* game_DanBlaze.c
*
*  Created on: Mar 26, 2015
*      Author: Dan
*/

#include "system.h"
#include "random_int.h"
#include "strings.h"

#define MAP_WIDTH 20
#define MAP_HEIGHT 30
#define ENEMY_MOVE_DELAY 500
#define SHOT_SPEED 25
#define MIN_ENEMY_SHOT_RATE 1000
#define MAX_ENEMY_SHOT_RATE 100
#define ENEMY_SHOT_SPEED 50

const int ROWS_OF_ENEMIES = 5;
const int COLUMNS_OF_ENEMIES = 15;

const int ENEMY_START_X = 3;
const int ENEMY_START_Y = 5;

#define NUMBER_OF_ENEMIES ROWS_OF_ENEMIES * COLUMNS_OF_ENEMIES

struct DanBlaze_game_t
{
    char x; // X coordinate of gun
    char c; //Character of gun
    int score; //how many ships were killed
    uint8_t id; //ID of game
};
struct  DanBlaze_game_t game;

struct Enemy_Ships_t
{
    int id;
    int x;
    int y;
    char c;
    int Alive;
};

struct Enemy_Ships_t Enemy_Ships[75];

struct Shot_t
{
    char c;
    int status;
    int y;
    int x;
};

struct Shot_t shot;

struct Enemy_Shot_t
{
    char c;
    int status;
    int y;
    int x;
};

struct Enemy_Shot_t enemyShot;

enum game_states {
    IDLE, START
};


#define NUMBER
static void Callback(int argc, char * argv[]);
static void Receiver(char c);

static void PrePlay(void);
static void Play(void);
static void Help(void);
static void MoveLeft(void);
static void MoveRight(void);
static void Fire(void);
static void MoveShotUp(void);
static void EnemyFireShot(void);
static void MoveEnemyShotDown(void);
static void GameOver(int victory);

static int FurthestEnemyRight(void);
static int FurthestEnemyLeft(void);
static int LowestEnemy(void);

static void MoveEnemies();
static void MoveEnemiesRight(void);
static void MoveEnemiesLeft(void);
static void MoveEnemiesDown(void);

static void CheckCollisionEnemy(void);
static int CheckWin(void);

static void CheckEnemyShotBoundary(void);
static void CheckEnemyShotKill(void);

void DanBlaze_Init()
{
    game.id = Game_Register("DBG", "Galaga", PrePlay, Help);
}

void PrePlay(void)
{
    //Game_ClearScreen();
    Game_Printf("Welcome to Galaxian!\n");
    Game_Printf("Controls:\n K - move right\n L - move left\n Space Bar - Fire\n Be careful, enemies will shoot back at you!");

    //Game_Printf("Use K and L to move Left and Right. Space bar fires");
    Task_Schedule(Play, 0, 1000, 0);
}

void Play(void)
{
    int i, j, k;

    //int NumberOfEnemies = ROWS_OF_ENEMIES * COLUMNS_OF_ENEMIES;
    //int NumberOfEnemies = 80;
    Game_ClearScreen();
    //Game_DrawRect(0,0,MAP_WIDTH, MAP_HEIGHT);

    //Initialize Game variables
    game.x = MAP_WIDTH / 2;
    game.c = 'W';
    game.score = 0;

    shot.c = '|';
    shot.status = 0;
    shot.x = MAP_WIDTH / 2;
    shot.y = MAP_HEIGHT -2;

    enemyShot.c = '|';
    enemyShot.status = 0;

    //Draw the gun
    Game_CharXY(game.c, game.x, MAP_HEIGHT - 1);

    //Draw the initial shot
    Game_CharXY(shot.c, shot.x, shot.y);

    Game_RegisterPlayer1Receiver(Receiver);
    Game_HideCursor();

    //Initilize enemy ships
    int x_coords = ENEMY_START_X;
    int y_coords = ENEMY_START_Y;
    for(i = 0; i < NUMBER_OF_ENEMIES; i++)
    {
        Enemy_Ships[i].x = x_coords;
        Enemy_Ships[i].y = y_coords;
        Enemy_Ships[i].c = 'X';
        Enemy_Ships[i].Alive = 1;

        //Reached end of row, restart from ENEMY_START_X
        if((x_coords - ENEMY_START_X + 1) == COLUMNS_OF_ENEMIES)
        {
            x_coords =ENEMY_START_X;
            y_coords++;
        }
        else
        {
            x_coords++;
        }
    }

    //Draw enemy ships
    for(i = 0; i < NUMBER_OF_ENEMIES; i++)
    {
        Game_CharXY(Enemy_Ships[i].c, Enemy_Ships[i].x, Enemy_Ships[i].y);
    }
    
    //Begin moving enemies
    Task_Schedule(MoveEnemiesRight,0, 1000, 0);

    //Let enemies start shooting
    Task_Schedule(EnemyFireShot, 0, 2000, 0);
}

//Check the furthest right enemy.
//If the enemies on the right are killed off, move remaining enemies further to the right (to the edge of the map)
//Returns the X value of the enemy that is furthest right.
int FurthestEnemyRight(void)
{
    int i;
    int x_coord = 0;
    for(i = 0; i < NUMBER_OF_ENEMIES; i++)
    {
        if((Enemy_Ships[i].x > x_coord) & Enemy_Ships[i].Alive == 1)
        {
            x_coord = Enemy_Ships[i].x;
        }
    }
    return x_coord;
}

//Check the furthest left enemy
//If enemies on the left are killed off, move reminaing enemies further to the left
//Returns the X of the enemy that is alive, and furthest left
int FurthestEnemyLeft(void)
{
    int i;
    int x_coord = MAP_WIDTH;
    for(i = 0; i < NUMBER_OF_ENEMIES; i++)
    {
        if((Enemy_Ships[i].x < x_coord) & Enemy_Ships[i].Alive == 1)
        {
            x_coord = Enemy_Ships[i].x;
        }
    }
return x_coord;
}

//Check the y coordinate of the lowest enemy
//Step through all rows of enemies to find the lowest alive enemy
//This is used to check that the lowest enemy doesn't hit the gun
//If the lowest alive enemy reaches the gun, the game is over
int LowestEnemy(void)
{
    int i;
    int y_coord = 0;
    for(i = 0; i < NUMBER_OF_ENEMIES; i++)
    {
        if((Enemy_Ships[i].y > y_coord) & Enemy_Ships[i].Alive == 1)
        {
            y_coord = Enemy_Ships[i].y;
        }
    }
return y_coord;
}

void MoveEnemiesRight(void)
{
    int i;
    int max_x_value = FurthestEnemyRight();
    if(max_x_value < MAP_WIDTH - 1)
    {
        for(i = 0; i < NUMBER_OF_ENEMIES; i++)
        {
            //if(Enemy_Ships[i].Alive == 1)
            //{
            Enemy_Ships[i].x++;
            Game_CharXY(Enemy_Ships[i].c, Enemy_Ships[i].x, Enemy_Ships[i].y);
            //}
         }
        for(i = 0; i<= NUMBER_OF_ENEMIES; i+= COLUMNS_OF_ENEMIES)
        {
            Game_CharXY(' ', Enemy_Ships[i].x - 1, Enemy_Ships[i].y);
        }
        Task_Schedule(MoveEnemiesRight, 0, 1000, 0);
    }
    else
    {
        Task_Schedule(MoveEnemiesDown,0,1,0);
    }
//Remove the ships to the left
}

void MoveEnemiesLeft(void)
{
    int i;
    int min_x_value = FurthestEnemyLeft();
    if(min_x_value > 1)
    {
        for(i = 0; i < NUMBER_OF_ENEMIES; i++)
        {
        //if(Enemy_Ships[i].Alive == 1)
        //{
            Enemy_Ships[i].x--;
            Game_CharXY(Enemy_Ships[i].c, Enemy_Ships[i].x, Enemy_Ships[i].y);
        //}
        }

        for(i = COLUMNS_OF_ENEMIES - 1; i < NUMBER_OF_ENEMIES; i+= COLUMNS_OF_ENEMIES)
        {
            Game_CharXY(' ', Enemy_Ships[i].x+1, Enemy_Ships[i].y);
        }
        
        Task_Schedule(MoveEnemiesLeft,0,1000,0);
    }
    else
    {
        Task_Schedule(MoveEnemiesDown,0,1,0);
    }
}


void MoveEnemiesDown(void)
{
    int i;
    int max_y_value = LowestEnemy();
    if(max_y_value < MAP_HEIGHT)
    {
        for(i = 0; i< NUMBER_OF_ENEMIES; i++)
        {
            //if(Enemy_Ships[i].Alive == 1)
            //{
            Enemy_Ships[i].y++;
            Game_CharXY(Enemy_Ships[i].c, Enemy_Ships[i].x, Enemy_Ships[i].y);
            //}
        }

        for(i = 0; i < COLUMNS_OF_ENEMIES; i++)
        {
            Game_CharXY(' ', Enemy_Ships[i].x, Enemy_Ships[i].y-1);
        }
    }

    //Determine whether to move enemies left or right
    int x_value = FurthestEnemyRight();
    if (x_value + 1 == MAP_WIDTH)
    {
        //Enemies are on teh right. begin moving them left
        Task_Schedule(MoveEnemiesLeft,0,1000,0);
    }
    else
    {
        Task_Schedule(MoveEnemiesRight,0,1000,0);
    }
}

void Help(void)
{
    Game_Printf("Lol good luck");
}

void Receiver(char c)
{
    switch(c)
    {
        case 'k':
            MoveLeft();
            break;

        case 'l':
            MoveRight();
            break;

        case ' ':
            Fire();
            break;
    }
}

void MoveLeft(void)
{
    if(game.x > 1)
    {
        Game_CharXY(' ', game.x, MAP_HEIGHT - 1);
        game.x--;
        Game_CharXY(game.c, game.x, MAP_HEIGHT - 1);

        if(shot.status == 0)
        {
            Game_CharXY(' ', shot.x, shot.y);
            shot.x--;
            Game_CharXY(shot.c, shot.x, shot.y);
        }
    }
}

void MoveRight(void)
{
    if(game.x < MAP_WIDTH - 1)
    {
        Game_CharXY(' ' , game.x, MAP_HEIGHT - 1);
        game.x++;
        Game_CharXY(game.c, game.x, MAP_HEIGHT - 1);

        if(shot.status == 0)
        {
            Game_CharXY(' ', shot.x, shot.y);
            shot.x++;
            Game_CharXY(shot.c, shot.x, shot.y);
        }
    }
}

void Fire(void)
{
    //Set shot status to 1 to indicate that the shot has been fired
    if(shot.status ==0)
    {
        shot.status = 1;
        Task_Schedule(MoveShotUp, 0, SHOT_SPEED, SHOT_SPEED);
    }
}

void MoveShotUp(void)
{
    if(shot.status == 1)
    {
        if( shot.y > 1)
        {
            Game_CharXY(' ', shot.x, shot.y);
            shot.y--;
            Game_CharXY(shot.c, shot.x, shot.y);
        }

        //Shot has reached the top of the map. Delete the shot
        else
        {
            Game_CharXY(' ', shot.x, shot.y);
            Task_Remove(MoveShotUp,0);
            shot.x = game.x;
            shot.y = MAP_HEIGHT - 2;
            Game_CharXY(shot.c, shot.x, shot.y);
            shot.status = 0;
        }
    }
CheckCollisionEnemy();
}

//Check if the shot has collided with an enemy
void CheckCollisionEnemy()
{
    int i;
    for(i = 0; i< NUMBER_OF_ENEMIES; i++)
    {
        if((shot.x == Enemy_Ships[i].x) && (shot.y == Enemy_Ships[i].y) && Enemy_Ships[i].Alive == 1 && shot.status == 1)
        {
            Enemy_Ships[i].Alive = 0;
            Enemy_Ships[i].c = ' ';
            Game_CharXY(' ', shot.x, shot.y);
            Task_Remove(MoveShotUp,0);
            shot.x = game.x;
            shot.y = MAP_HEIGHT - 2;
            Game_CharXY(shot.c, shot.x, shot.y);
            shot.status = 0;
            game.score++;
        }
    }
    CheckWin();
}

//Check for a win condition. All enemy ships destroyed. Return 1 if true.
int CheckWin(void)
{
    int i;
    int win = 1;
    for(i = 0; i < NUMBER_OF_ENEMIES; i++)
    {
        if(Enemy_Ships[i].Alive == 1)
        {
            win = 0;
        }
    }

    if(win == 1)
    {
        GameOver(win);
    }
}

void EnemyFireShot(void)
{
    //No shots are moving. Spawn a new shot, and begin it moving down
    if(enemyShot.status == 0)
    {
        //Pick a random x coordinate, between Furthest Enemy Right, and Furthest Enemy Left
        enemyShot.x = random_int(FurthestEnemyLeft(), FurthestEnemyRight());

    //Pick the Y coordinate to be below Lowest Enemy
    //This prevents the shot from overwriting a ship
        enemyShot.y = LowestEnemy() + 1;

        Task_Schedule(MoveEnemyShotDown, 0, ENEMY_SHOT_SPEED, ENEMY_SHOT_SPEED);
    }


}

void MoveEnemyShotDown(void)
{
    Game_CharXY(' ', enemyShot.x, enemyShot.y);
    enemyShot.y++;
    Game_CharXY('|', enemyShot.x, enemyShot.y);
    CheckEnemyShotBoundary();
    CheckEnemyShotKill();
}

//Make sure the enemy shot doesnt go out of bounds. If it does, schedule another shot to fire
void CheckEnemyShotBoundary(void)
{
    if(enemyShot.y > MAP_HEIGHT - 1)
    {
        //Delete the shot
        Task_Remove(MoveEnemyShotDown,0);
        Game_CharXY(' ', enemyShot.x, enemyShot.y);

        //Spawn a new shot to be fired
       Task_Schedule(EnemyFireShot,0, random_int(MIN_ENEMY_SHOT_RATE, MAX_ENEMY_SHOT_RATE),0);

    }
}

//See if the enemy has hit the plyers gun. If so, Game over
void CheckEnemyShotKill(void)
{
    if(enemyShot.x == game.x && enemyShot.y == MAP_HEIGHT - 1)
    {
        Task_Remove(MoveEnemyShotDown,0);
        Game_CharXY(' ', shot.x, shot.y);
        Game_CharXY('L', 3, 3);
        GameOver(0);
    }
}

void GameOver(int victory)
{
    

    //Clean up after ourselves
    Game_ClearScreen();

    Task_Remove(MoveEnemiesDown,0);
    Task_Remove(MoveEnemiesRight,0);
    Task_Remove(MoveEnemiesLeft,0);
    Task_Remove(EnemyFireShot,0);
    Task_Remove(MoveEnemyShotDown,0);


        if(victory == 0)
        {
            Game_Printf("Game Over! Thanks for playing!");
        }
        if(victory == 1)
        {
            Game_Printf("You won!");
        }
    
}
    