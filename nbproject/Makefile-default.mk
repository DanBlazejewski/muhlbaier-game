#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Muhlbaier_Game.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Muhlbaier_Game.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../../Reusable/hal/PIC32MX/PIC32MX250F128B/hal_uart.c main.c configuration_bits.c ../../Reusable/src/uart.c ../../Reusable/src/buffer.c ../../Reusable/src/charReceiverList.c ../../Reusable/src/task.c ../../Reusable/src/buffer_printf.c ../../Reusable/src/timing.c ../../Reusable/src/subsys.c ../../Reusable/src/game.c ../../Reusable/src/games/game_MUH3.c ../../Reusable/src/random_int.c ../../Reusable/src/list.c ../../Reusable/src/itemBuffer.c ../../Reusable/src/terminal.c game_DanBlaze.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/2031239293/hal_uart.o ${OBJECTDIR}/main.o ${OBJECTDIR}/configuration_bits.o ${OBJECTDIR}/_ext/1884272512/uart.o ${OBJECTDIR}/_ext/1884272512/buffer.o ${OBJECTDIR}/_ext/1884272512/charReceiverList.o ${OBJECTDIR}/_ext/1884272512/task.o ${OBJECTDIR}/_ext/1884272512/buffer_printf.o ${OBJECTDIR}/_ext/1884272512/timing.o ${OBJECTDIR}/_ext/1884272512/subsys.o ${OBJECTDIR}/_ext/1884272512/game.o ${OBJECTDIR}/_ext/1786453902/game_MUH3.o ${OBJECTDIR}/_ext/1884272512/random_int.o ${OBJECTDIR}/_ext/1884272512/list.o ${OBJECTDIR}/_ext/1884272512/itemBuffer.o ${OBJECTDIR}/_ext/1884272512/terminal.o ${OBJECTDIR}/game_DanBlaze.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/2031239293/hal_uart.o.d ${OBJECTDIR}/main.o.d ${OBJECTDIR}/configuration_bits.o.d ${OBJECTDIR}/_ext/1884272512/uart.o.d ${OBJECTDIR}/_ext/1884272512/buffer.o.d ${OBJECTDIR}/_ext/1884272512/charReceiverList.o.d ${OBJECTDIR}/_ext/1884272512/task.o.d ${OBJECTDIR}/_ext/1884272512/buffer_printf.o.d ${OBJECTDIR}/_ext/1884272512/timing.o.d ${OBJECTDIR}/_ext/1884272512/subsys.o.d ${OBJECTDIR}/_ext/1884272512/game.o.d ${OBJECTDIR}/_ext/1786453902/game_MUH3.o.d ${OBJECTDIR}/_ext/1884272512/random_int.o.d ${OBJECTDIR}/_ext/1884272512/list.o.d ${OBJECTDIR}/_ext/1884272512/itemBuffer.o.d ${OBJECTDIR}/_ext/1884272512/terminal.o.d ${OBJECTDIR}/game_DanBlaze.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/2031239293/hal_uart.o ${OBJECTDIR}/main.o ${OBJECTDIR}/configuration_bits.o ${OBJECTDIR}/_ext/1884272512/uart.o ${OBJECTDIR}/_ext/1884272512/buffer.o ${OBJECTDIR}/_ext/1884272512/charReceiverList.o ${OBJECTDIR}/_ext/1884272512/task.o ${OBJECTDIR}/_ext/1884272512/buffer_printf.o ${OBJECTDIR}/_ext/1884272512/timing.o ${OBJECTDIR}/_ext/1884272512/subsys.o ${OBJECTDIR}/_ext/1884272512/game.o ${OBJECTDIR}/_ext/1786453902/game_MUH3.o ${OBJECTDIR}/_ext/1884272512/random_int.o ${OBJECTDIR}/_ext/1884272512/list.o ${OBJECTDIR}/_ext/1884272512/itemBuffer.o ${OBJECTDIR}/_ext/1884272512/terminal.o ${OBJECTDIR}/game_DanBlaze.o

# Source Files
SOURCEFILES=../../Reusable/hal/PIC32MX/PIC32MX250F128B/hal_uart.c main.c configuration_bits.c ../../Reusable/src/uart.c ../../Reusable/src/buffer.c ../../Reusable/src/charReceiverList.c ../../Reusable/src/task.c ../../Reusable/src/buffer_printf.c ../../Reusable/src/timing.c ../../Reusable/src/subsys.c ../../Reusable/src/game.c ../../Reusable/src/games/game_MUH3.c ../../Reusable/src/random_int.c ../../Reusable/src/list.c ../../Reusable/src/itemBuffer.c ../../Reusable/src/terminal.c game_DanBlaze.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/Muhlbaier_Game.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX250F128B
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/2031239293/hal_uart.o: ../../Reusable/hal/PIC32MX/PIC32MX250F128B/hal_uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2031239293" 
	@${RM} ${OBJECTDIR}/_ext/2031239293/hal_uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/2031239293/hal_uart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2031239293/hal_uart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/2031239293/hal_uart.o.d" -o ${OBJECTDIR}/_ext/2031239293/hal_uart.o ../../Reusable/hal/PIC32MX/PIC32MX250F128B/hal_uart.c    -w
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/main.o.d" -o ${OBJECTDIR}/main.o main.c    -w
	
${OBJECTDIR}/configuration_bits.o: configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/configuration_bits.o.d 
	@${RM} ${OBJECTDIR}/configuration_bits.o 
	@${FIXDEPS} "${OBJECTDIR}/configuration_bits.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/configuration_bits.o.d" -o ${OBJECTDIR}/configuration_bits.o configuration_bits.c    -w
	
${OBJECTDIR}/_ext/1884272512/uart.o: ../../Reusable/src/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/uart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/uart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/uart.o.d" -o ${OBJECTDIR}/_ext/1884272512/uart.o ../../Reusable/src/uart.c    -w
	
${OBJECTDIR}/_ext/1884272512/buffer.o: ../../Reusable/src/buffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/buffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/buffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/buffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/buffer.o.d" -o ${OBJECTDIR}/_ext/1884272512/buffer.o ../../Reusable/src/buffer.c    -w
	
${OBJECTDIR}/_ext/1884272512/charReceiverList.o: ../../Reusable/src/charReceiverList.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/charReceiverList.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/charReceiverList.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/charReceiverList.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/charReceiverList.o.d" -o ${OBJECTDIR}/_ext/1884272512/charReceiverList.o ../../Reusable/src/charReceiverList.c    -w
	
${OBJECTDIR}/_ext/1884272512/task.o: ../../Reusable/src/task.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/task.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/task.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/task.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/task.o.d" -o ${OBJECTDIR}/_ext/1884272512/task.o ../../Reusable/src/task.c    -w
	
${OBJECTDIR}/_ext/1884272512/buffer_printf.o: ../../Reusable/src/buffer_printf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/buffer_printf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/buffer_printf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/buffer_printf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/buffer_printf.o.d" -o ${OBJECTDIR}/_ext/1884272512/buffer_printf.o ../../Reusable/src/buffer_printf.c    -w
	
${OBJECTDIR}/_ext/1884272512/timing.o: ../../Reusable/src/timing.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/timing.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/timing.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/timing.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/timing.o.d" -o ${OBJECTDIR}/_ext/1884272512/timing.o ../../Reusable/src/timing.c    -w
	
${OBJECTDIR}/_ext/1884272512/subsys.o: ../../Reusable/src/subsys.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/subsys.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/subsys.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/subsys.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/subsys.o.d" -o ${OBJECTDIR}/_ext/1884272512/subsys.o ../../Reusable/src/subsys.c    -w
	
${OBJECTDIR}/_ext/1884272512/game.o: ../../Reusable/src/game.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/game.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/game.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/game.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/game.o.d" -o ${OBJECTDIR}/_ext/1884272512/game.o ../../Reusable/src/game.c    -w
	
${OBJECTDIR}/_ext/1786453902/game_MUH3.o: ../../Reusable/src/games/game_MUH3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1786453902" 
	@${RM} ${OBJECTDIR}/_ext/1786453902/game_MUH3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1786453902/game_MUH3.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1786453902/game_MUH3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1786453902/game_MUH3.o.d" -o ${OBJECTDIR}/_ext/1786453902/game_MUH3.o ../../Reusable/src/games/game_MUH3.c    -w
	
${OBJECTDIR}/_ext/1884272512/random_int.o: ../../Reusable/src/random_int.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/random_int.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/random_int.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/random_int.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/random_int.o.d" -o ${OBJECTDIR}/_ext/1884272512/random_int.o ../../Reusable/src/random_int.c    -w
	
${OBJECTDIR}/_ext/1884272512/list.o: ../../Reusable/src/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/list.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/list.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/list.o.d" -o ${OBJECTDIR}/_ext/1884272512/list.o ../../Reusable/src/list.c    -w
	
${OBJECTDIR}/_ext/1884272512/itemBuffer.o: ../../Reusable/src/itemBuffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/itemBuffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/itemBuffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/itemBuffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/itemBuffer.o.d" -o ${OBJECTDIR}/_ext/1884272512/itemBuffer.o ../../Reusable/src/itemBuffer.c    -w
	
${OBJECTDIR}/_ext/1884272512/terminal.o: ../../Reusable/src/terminal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/terminal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/terminal.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/terminal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/terminal.o.d" -o ${OBJECTDIR}/_ext/1884272512/terminal.o ../../Reusable/src/terminal.c    -w
	
${OBJECTDIR}/game_DanBlaze.o: game_DanBlaze.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/game_DanBlaze.o.d 
	@${RM} ${OBJECTDIR}/game_DanBlaze.o 
	@${FIXDEPS} "${OBJECTDIR}/game_DanBlaze.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/game_DanBlaze.o.d" -o ${OBJECTDIR}/game_DanBlaze.o game_DanBlaze.c    -w
	
else
${OBJECTDIR}/_ext/2031239293/hal_uart.o: ../../Reusable/hal/PIC32MX/PIC32MX250F128B/hal_uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2031239293" 
	@${RM} ${OBJECTDIR}/_ext/2031239293/hal_uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/2031239293/hal_uart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2031239293/hal_uart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/2031239293/hal_uart.o.d" -o ${OBJECTDIR}/_ext/2031239293/hal_uart.o ../../Reusable/hal/PIC32MX/PIC32MX250F128B/hal_uart.c    -w
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/main.o.d" -o ${OBJECTDIR}/main.o main.c    -w
	
${OBJECTDIR}/configuration_bits.o: configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/configuration_bits.o.d 
	@${RM} ${OBJECTDIR}/configuration_bits.o 
	@${FIXDEPS} "${OBJECTDIR}/configuration_bits.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/configuration_bits.o.d" -o ${OBJECTDIR}/configuration_bits.o configuration_bits.c    -w
	
${OBJECTDIR}/_ext/1884272512/uart.o: ../../Reusable/src/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/uart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/uart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/uart.o.d" -o ${OBJECTDIR}/_ext/1884272512/uart.o ../../Reusable/src/uart.c    -w
	
${OBJECTDIR}/_ext/1884272512/buffer.o: ../../Reusable/src/buffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/buffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/buffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/buffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/buffer.o.d" -o ${OBJECTDIR}/_ext/1884272512/buffer.o ../../Reusable/src/buffer.c    -w
	
${OBJECTDIR}/_ext/1884272512/charReceiverList.o: ../../Reusable/src/charReceiverList.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/charReceiverList.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/charReceiverList.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/charReceiverList.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/charReceiverList.o.d" -o ${OBJECTDIR}/_ext/1884272512/charReceiverList.o ../../Reusable/src/charReceiverList.c    -w
	
${OBJECTDIR}/_ext/1884272512/task.o: ../../Reusable/src/task.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/task.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/task.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/task.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/task.o.d" -o ${OBJECTDIR}/_ext/1884272512/task.o ../../Reusable/src/task.c    -w
	
${OBJECTDIR}/_ext/1884272512/buffer_printf.o: ../../Reusable/src/buffer_printf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/buffer_printf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/buffer_printf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/buffer_printf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/buffer_printf.o.d" -o ${OBJECTDIR}/_ext/1884272512/buffer_printf.o ../../Reusable/src/buffer_printf.c    -w
	
${OBJECTDIR}/_ext/1884272512/timing.o: ../../Reusable/src/timing.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/timing.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/timing.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/timing.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/timing.o.d" -o ${OBJECTDIR}/_ext/1884272512/timing.o ../../Reusable/src/timing.c    -w
	
${OBJECTDIR}/_ext/1884272512/subsys.o: ../../Reusable/src/subsys.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/subsys.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/subsys.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/subsys.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/subsys.o.d" -o ${OBJECTDIR}/_ext/1884272512/subsys.o ../../Reusable/src/subsys.c    -w
	
${OBJECTDIR}/_ext/1884272512/game.o: ../../Reusable/src/game.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/game.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/game.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/game.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/game.o.d" -o ${OBJECTDIR}/_ext/1884272512/game.o ../../Reusable/src/game.c    -w
	
${OBJECTDIR}/_ext/1786453902/game_MUH3.o: ../../Reusable/src/games/game_MUH3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1786453902" 
	@${RM} ${OBJECTDIR}/_ext/1786453902/game_MUH3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1786453902/game_MUH3.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1786453902/game_MUH3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1786453902/game_MUH3.o.d" -o ${OBJECTDIR}/_ext/1786453902/game_MUH3.o ../../Reusable/src/games/game_MUH3.c    -w
	
${OBJECTDIR}/_ext/1884272512/random_int.o: ../../Reusable/src/random_int.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/random_int.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/random_int.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/random_int.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/random_int.o.d" -o ${OBJECTDIR}/_ext/1884272512/random_int.o ../../Reusable/src/random_int.c    -w
	
${OBJECTDIR}/_ext/1884272512/list.o: ../../Reusable/src/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/list.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/list.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/list.o.d" -o ${OBJECTDIR}/_ext/1884272512/list.o ../../Reusable/src/list.c    -w
	
${OBJECTDIR}/_ext/1884272512/itemBuffer.o: ../../Reusable/src/itemBuffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/itemBuffer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/itemBuffer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/itemBuffer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/itemBuffer.o.d" -o ${OBJECTDIR}/_ext/1884272512/itemBuffer.o ../../Reusable/src/itemBuffer.c    -w
	
${OBJECTDIR}/_ext/1884272512/terminal.o: ../../Reusable/src/terminal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1884272512" 
	@${RM} ${OBJECTDIR}/_ext/1884272512/terminal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1884272512/terminal.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1884272512/terminal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/_ext/1884272512/terminal.o.d" -o ${OBJECTDIR}/_ext/1884272512/terminal.o ../../Reusable/src/terminal.c    -w
	
${OBJECTDIR}/game_DanBlaze.o: game_DanBlaze.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/game_DanBlaze.o.d 
	@${RM} ${OBJECTDIR}/game_DanBlaze.o 
	@${FIXDEPS} "${OBJECTDIR}/game_DanBlaze.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../../Reusable/include" -I"../../Reusable/hal/hal_includes" -I"../Muhlbaier Game.X" -I"../../Reusable/hal/PIC32MX/PIC32MX250F128B" -MMD -MF "${OBJECTDIR}/game_DanBlaze.o.d" -o ${OBJECTDIR}/game_DanBlaze.o game_DanBlaze.c    -w
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Muhlbaier_Game.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Muhlbaier_Game.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}           -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC00490:0x1FC00BEF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/Muhlbaier_Game.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Muhlbaier_Game.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/Muhlbaier_Game.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
