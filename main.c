/* 
 * File:   main.c
 * Author: Dan
 *
 * Created on March 24, 2015, 1:39 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "system.h"
#include <plib.h>
#include "library.h"
#include "hal_settings.h"
#include <xc.h>

/* test
 * 
 */

#define UART_CHANNEL 2


int main(int argc, char** argv) {

    SYSTEMConfig(FCPU, SYS_CFG_ALL); // config clock for PIC32
	// WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer for MSP430

    Timing_Init(); // initialize the timing module first
    Task_Init(); // initialize task managemnet module next
    UART_Init(UART_CHANNEL);
    SystemInit();
    Game_Init();
    MUH3_Init();
    DanBlaze_Init();

	// enable interrupts after modules using interrupts have been initialized
	EnableInterrupts();
        //Task_Schedule(hello_world, 0, 1000, 2000);
        
    
    while(1) SystemTick();
}

